FROM docker.io/bitnami/wordpress:5.8.0

ENV PATH="/my_data/bin:${PATH}"

USER root
COPY bin /my_data/bin
RUN chmod -R +x /my_data/bin/*
COPY wordpress /my_data/wordpress