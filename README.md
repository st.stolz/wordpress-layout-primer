# KIT-DLT Homepage

## Init Dev Environment

When init the project the first time do:

* `docker-compose up -d`
* `docker exec -ti kit-wordpress init-kit-wordpress.bash`

## Update Wordpress

If new Version in Dockerfile:

* `docker-compose down`
* `docker-compose build`
* `docker-compose up`

## Usage

* Admin Area: [http://localhost/wp-login.php](http://localhost/wp-login.php)

## Custom Wordpress Layouts

* [Custom Theme erstellen](https://websitesetup.org/wordpress-theme-development/)